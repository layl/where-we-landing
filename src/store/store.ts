import { configureStore } from "@reduxjs/toolkit";
import { SettingsAction, SET_ACCESS_KEY, SET_ICAO_POSITIONS, IcaosAction } from "src/store/actions";
import { IcaoEntry } from "src/utils/icaos";

export type SettingsState = {
    accessKey: string | null;
}

const settingsInitialState: SettingsState = {
    accessKey: null,
}

const settingsReducer = (state = settingsInitialState, action: SettingsAction) => {
    switch (action.type) {
        case SET_ACCESS_KEY:
            return {
                ...state,
                accessKey: action.accessKey,
            };
        default:
            return state;
    }
}

export type IcaosState = {
    icaoEntries: Record<string, IcaoEntry>;
}

const icaosInitialState: IcaosState = {
    icaoEntries: {},
}

const icaosReducer = (state = icaosInitialState, action: IcaosAction) => {
    switch (action.type) {
        case SET_ICAO_POSITIONS:
            return {
                ...state,
                icaoEntries: action.icaoEntries,
            }
        default:
            return state;
    }
}

export type AppState = {
    settings: SettingsState;
    icaos: IcaosState;
}

const store = configureStore<AppState>({
    reducer: {
        settings: settingsReducer,
        icaos: icaosReducer,
    },
    middleware: [] as any
});

export default store;