import React, { FC } from "react";
import { FormControl, FormLabel, Input, FormErrorMessage, Button, Alert, AlertIcon } from "@chakra-ui/core";
import { Formik, Field, FieldProps } from "formik";
import { useSelector } from "react-redux";

import { AppState } from "src/store/store";
import { IcaoEntry } from "src/utils/icaos";

type StartBarProps = {
    onOpenIcao: (icao: string) => void;
}

type FindForm = {
    icao: string;
}

const StartBar: FC<StartBarProps> = (props) => {
    const icaoEntries = useSelector<AppState, Record<string, IcaoEntry>>(
        state => state.icaos.icaoEntries
    );

    function validateIcao(value: string) {
        if (icaoEntries[value] == undefined) {
            return "ICAO does not exist.";
        }

        return null;
    }

    function onFindSubmit(values: FindForm) {
        props.onOpenIcao(values.icao);
    }

    return (
        <Formik<FindForm> initialValues={{ icao: "" }} onSubmit={onFindSubmit}>
            {(formikProps) => (
                <form onSubmit={formikProps.handleSubmit}>
                    <Field name="icao" validate={validateIcao}>
                        {({ field, form }: FieldProps<string, FindForm>) => (
                            <FormControl isInvalid={!!form.errors.icao}>
                                <FormLabel>ICAO</FormLabel>
                                <Input
                                    {...field}
                                    type="text"
                                    onChange={e => {
                                        let value = e.target.value || "";
                                        value = value.toUpperCase().trim();
                                        formikProps.setFieldValue(field.name, value);
                                    }}
                                />
                                <FormErrorMessage>{form.errors.icao}</FormErrorMessage>
                            </FormControl>
                        )}
                    </Field>
                    <Button
                        type="submit"
                        colorScheme="blue"
                        width="full"
                        mt={2}
                        disabled={!formikProps.isValid || !formikProps.dirty}
                    >
                        Find
                    </Button>
                </form>
            )}
        </Formik>
    );
}

export default StartBar;