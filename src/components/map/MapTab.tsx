import React, { FC, useRef, useEffect, MutableRefObject, useState, Dispatch, SetStateAction, } from "react"
import leaflet from "leaflet";
import { Box } from "@chakra-ui/core";

import { IcaoEntry } from "src/utils/icaos";
import MapSideBar from "src/components/map/MapSideBar";
import { useSelector } from "react-redux";
import { AppState } from "src/store/store";

class MapManager {
    map: leaflet.Map;
    iconClear: leaflet.Icon;
    iconHighlighted: leaflet.Icon;

    icaoEntries: Record<string, IcaoEntry> = {};
    activeMarkers: Record<string, {marker: leaflet.Marker, highlighted: boolean}> = {};
    highlightedIcaos: Set<string> = new Set();

    constructor(mapRef: MutableRefObject<HTMLDivElement>) {
        this.map = leaflet.map(mapRef.current).setView([0.0, 0.0], 2);

        // Add the OpenStreetMap layer
        var carto = leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
        });
        carto.addTo(this.map);

        // Load the marker icons
        this.iconClear = leaflet.icon({
            iconUrl: "../static/pin-clear.png",
            iconSize: [18, 18],
            iconAnchor: [9, 9],
        });
        this.iconHighlighted = leaflet.icon({
            iconUrl: "../static/pin-highlighted.png",
            iconSize: [18, 18],
            iconAnchor: [9, 9],
        });

        this.map.addEventListener("moveend", event => {
            this.updateMarkers();
        });

        this.map.addEventListener("zoomend", event => {
            this.updateMarkers();
        });
    }

    cleanup() {
        this.map.remove();
    }

    updateMarkers() {
        // Go over all icaos
        for (const icao in this.icaoEntries) {
            const position = this.icaoEntries[icao];
            const bounds = this.map.getBounds();
            const highlighted = this.highlightedIcaos.has(icao);

            // Check if this icao is within the viewport
            const inViewport =
                position.longitude >= bounds.getWest() &&
                position.longitude <= bounds.getEast() &&
                position.latitude >= bounds.getSouth() &&
                position.latitude <= bounds.getNorth();

            // Check if this icao should be visible based on the zoom level
            const zoomVisible = this.map.getZoom() >= 8 || highlighted;

            if (inViewport && zoomVisible) {
                const icon = highlighted ? this.iconHighlighted : this.iconClear;

                // Check if it is visible already
                const activeMarker = this.activeMarkers[icao];
                if (activeMarker != undefined) {
                    // Make sure the icon's higlighted correctly
                    if (activeMarker.highlighted != highlighted) {
                        activeMarker.marker.setIcon(icon);
                        activeMarker.highlighted = highlighted;
                    }

                    continue;
                }

                // Add the marker
                let marker = leaflet.marker(
                    [position.latitude, position.longitude],
                    { icon: icon }
                );

                marker.addTo(this.map);
                this.activeMarkers[icao] = { marker: marker, highlighted: highlighted };
            } else {
                // Check if it is hidden already
                if (this.activeMarkers[icao] == undefined) { 
                    // Nothing to do
                    continue;
                }

                // Remove this marker
                this.activeMarkers[icao].marker.remove();
                delete this.activeMarkers[icao];
            }
        }
    }

    setIcaoEntries(icaoEntries: Record<string, IcaoEntry>) {
        this.icaoEntries = icaoEntries;
        this.updateMarkers();
    }

    focusOn(icao: string) {
        const position = this.icaoEntries[icao];
        this.map.setView([position.latitude, position.longitude], 8);
    }

    highlightIcaos(icaos: string[]) {
        this.highlightedIcaos = new Set(icaos);
        this.updateMarkers();
    }
}

type MapContainer = {
    mapManager: MapManager | null;
}

function mapEffect(
    mapRef: MutableRefObject<HTMLDivElement>,
    mapContainer: MapContainer,
    setMapContainer: Dispatch<SetStateAction<MapContainer>>,
) {
    const mapManager = new MapManager(mapRef);

    mapContainer.mapManager = mapManager;
    setMapContainer(mapContainer);

    return () => mapManager.cleanup();
}

const MapTab: FC = () => {
    const [mapContainer, setMapContainer] = useState<MapContainer>({ mapManager: null });
    const mapRef = useRef(null);

    // Set up the map itself
    useEffect(() => mapEffect(mapRef, mapContainer, setMapContainer), []);

    // Keep map positions up to date
    const icaoEntries = useSelector<AppState, Record<string, IcaoEntry>>(
        state => state.icaos.icaoEntries,
    );
    useEffect(() => {
        mapContainer.mapManager?.setIcaoEntries(icaoEntries);
    }, [icaoEntries, mapContainer]);

    function onOpenIcao(icao: string) {
        mapContainer.mapManager.highlightIcaos([icao]);
        mapContainer.mapManager.focusOn(icao);
    }

    function onClearMap() {
        mapContainer.mapManager.highlightIcaos([]);
    }

    return (
        <Box display="flex" height="100%">
            <Box as="div" flexGrow={1} ref={mapRef} />
            <Box width="300px" overflow="auto">
                <MapSideBar onOpenIcao={onOpenIcao} onClearMap={onClearMap} />
            </Box>
        </Box>
    );
}

export default MapTab;