import React, { Component, ChangeEvent, FormEvent } from "react";
import { Box, FormControl, FormLabel, Input, InputGroup, InputRightAddon, Button, Heading } from "@chakra-ui/core";
import { parseStringPromise } from "xml2js";
import { cloneDeep } from "lodash";
import got from "got";
import { connect } from "react-redux";

import GreatCircle from "src/utils/great_circle";
import { UserData, Assignment } from "src/utils/user_data";
import { AppState } from "src/store/store";
import { IcaoEntry, loadIcaos } from "src/utils/icaos";

function findIcaosWithinRange(
    icaoEntries: Record<string, IcaoEntry>,
    icao: string,
    nauticalMiles: number,
) {
    const icaoPosition = icaoEntries[icao];
    let foundIcaos = [];

    for (const otherIcao of Object.keys(icaoEntries)) {
        const otherIcaoPosition = icaoEntries[otherIcao];
        const distance = GreatCircle.distance(
            icaoPosition.latitude,
            icaoPosition.longitude,
            otherIcaoPosition.latitude,
            otherIcaoPosition.longitude,
        );

        if (distance <= nauticalMiles) {
            foundIcaos.push(otherIcao);
        }
    }

    return foundIcaos;
}

async function addAssignmentsWithXml(
    icaoAssignments: Record<string, Assignment[]>,
    xmlFile: string
) {
    // Parse the data
    const data = await parseStringPromise(xmlFile);

    // Transform the data to new entries
    for(const assignment of data.IcaoJobsFrom.Assignment) {
        if (assignment.Type[0] != "Trip-Only") {
            continue;
        }

        if (icaoAssignments[assignment.FromIcao] == undefined) {
            icaoAssignments[assignment.FromIcao] = [];
        }

        icaoAssignments[assignment.FromIcao].push({
            toIcao: assignment.ToIcao,
            pay: parseFloat(assignment.Pay),
        });
    }
}

type Route = {
    from: string;
    to: string;
    pay: number;
}

type LegacyTabState = {
    icaoEntries: Record<string, IcaoEntry>,
    icaoAssignments: Record<string, Assignment[]>,

    icao: string,
    nauticalMiles: string,

    airportsCount: string,
    requestsCount: string,

    isDownloading: boolean,
    foundRoutes: Route[],
}

class LegacyTab extends Component<{accessKey: string}, LegacyTabState> {
    constructor(props: {accessKey: string}) {
        super(props);

        this.state = {
            icaoEntries: {},
            icaoAssignments: {},

            icao: "",
            nauticalMiles: "",

            airportsCount: "N/A",
            requestsCount: "N/A",

            isDownloading: false,
            foundRoutes: [],
        }
    }

    async componentDidMount() {
        let icaoEntries = await loadIcaos();

        // Load the database from disk
        const icaoAssignments = await UserData.loadIcaoAssignments();

        this.setState({
            icaoEntries: icaoEntries,
            icaoAssignments: icaoAssignments
        });
    }

    onIcaoChange = (event: ChangeEvent<HTMLInputElement>) => {
        this.setState({ icao: event.target.value });
        this.updatePreview(event.target.value, this.state.nauticalMiles);
    }

    onNauticalMilesChange = (event: ChangeEvent<HTMLInputElement>) => {
        this.setState({ nauticalMiles: event.target.value });
        this.updatePreview(this.state.icao, event.target.value);
    }

    updatePreview = (icao: string, nauticalMilesString: string) => {
        // Filter by number
        if (!/^[-+]?(\d+|Infinity)$/.test(nauticalMilesString)) {
            this.setState({ airportsCount: "N/A", requestsCount: "N/A" });
            return;
        }
        const nauticalMiles = Number(nauticalMilesString);

        // Filter by existing ICAO
        if (this.state.icaoEntries[icao] == undefined) {
            this.setState({ airportsCount: "N/A", requestsCount: "N/A" });
            return;
        }

        // Search all other ICAOs for within range
        let icaos = findIcaosWithinRange(this.state.icaoEntries, icao, nauticalMiles);

        this.setState({
            airportsCount: String(icaos.length),
            requestsCount: String(Math.ceil(icaos.length / 30)),
        });
    }

    onDownloadSubmit = async (event: FormEvent) => {
        event.preventDefault();

        // Filter by number
        if (!/^[-+]?(\d+|Infinity)$/.test(this.state.nauticalMiles)) {
            return;
        }
        const nauticalMiles = Number(this.state.nauticalMiles);

        // Filter by existing ICAO
        if (this.state.icaoEntries[this.state.icao] == undefined) {
            return;
        }

        // Filter by access key
        if (this.props.accessKey == null) {
            return;
        }

        this.setState({ isDownloading: true });

        // Get the ICAOs we need to check
        const targetIcaos = findIcaosWithinRange(
            this.state.icaoEntries,
            this.state.icao,
            nauticalMiles
        );

        // Generate URLs
        let dataUrls = [];
        const amountRequests = Math.ceil(targetIcaos.length / 30);
        for (let i = 0; i < amountRequests; i++) {
            let icaosList = targetIcaos[i*30];

            const start = (i * 30) + 1;
            const end = ((i + 1) * 30);
            for (let x = start; (x < end && x < targetIcaos.length); x++) {
                icaosList += "-" + targetIcaos[x];
            }

            const url = "https://server.fseconomy.net/data?userkey=" +
                this.props.accessKey +
                "&format=xml&query=icao&search=jobsfrom&icaos=" +
                icaosList;

            dataUrls.push(url);
        }

        // Remove airports we're going to request
        const icaoAssignments = cloneDeep(this.state.icaoAssignments);
        for (const icao of targetIcaos) {
            delete icaoAssignments[icao];
        }

        // Request data from all the URLs
        for (const url of dataUrls) {
            const response = await got(url);

            await addAssignmentsWithXml(icaoAssignments, response.body);
            this.setState({ icaoAssignments: icaoAssignments });

            // Save the database to disk
            UserData.saveIcaoAssignments(icaoAssignments);
        }

        this.setState({ isDownloading: false });
    }

    onClearCLicked = () => {
        this.setState({ icaoAssignments: {} });
        UserData.saveIcaoAssignments({});
    }

    onFindClicked = () => {
        let allRoutes: Route[] = [];

        // Find all the routes
        for (const fromIcao in this.state.icaoAssignments) {
            const icaoAssignment = this.state.icaoAssignments[fromIcao];

            // Fill possible routes
            let possibleRoutes: Record<string, number> = {};
            for (const assignment of icaoAssignment) {
                if (possibleRoutes[assignment.toIcao] == undefined) {
                    possibleRoutes[assignment.toIcao] = assignment.pay;
                } else {
                    possibleRoutes[assignment.toIcao] += assignment.pay;
                }
            }

            // Store the total routes
            for (const toIcao in possibleRoutes) {
                allRoutes.push({
                    from: fromIcao,
                    to: toIcao,
                    pay: possibleRoutes[toIcao],
                })
            }
        }

        // Sort and store the top 10
        let foundRoutes = allRoutes
            .sort((a, b) => b.pay - a.pay)
            .slice(0, 10);

        this.setState({ foundRoutes: foundRoutes });
    }

    render() {
        const foundRoutes = this.state.foundRoutes.map(route => (
            <div key={route.from + route.to}>{route.from} to {route.to} for {route.pay}</div>
        ));

        return (
            <Box p={4}>
                <Heading size="lg">Download Assignments to Database</Heading>
                <form onSubmit={this.onDownloadSubmit}>
                    <FormControl>
                        <FormLabel>ICAO</FormLabel>
                        <Input type="text" onChange={this.onIcaoChange} />
                    </FormControl>
                    <FormControl>
                        <FormLabel>Range</FormLabel>
                        <InputGroup>
                            <Input type="text" onChange={this.onNauticalMilesChange} />
                            <InputRightAddon children="NM" />
                        </InputGroup>
                    </FormControl>
                    <Box mt={4}>
                        This will download {this.state.airportsCount} airports, using {this.state.requestsCount} data requests.
                    </Box>
                    <Button
                        type="submit"
                        mt={4}
                        colorScheme="blue"
                        isLoading={this.state.isDownloading}
                    >
                        Download
                    </Button>
                </form>

                <Heading size="lg" mt={4}>Assignment Database</Heading>
                <div>ICAOs: {Object.keys(this.state.icaoAssignments).length}</div>
                <Button colorScheme="blue" onClick={this.onClearCLicked}>Clear Database</Button>
                
                <Heading size="lg" mt={4}>Find Best</Heading>
                <Button colorScheme="blue" onClick={this.onFindClicked}>Find</Button>
                <div>{foundRoutes}</div>
            </Box>
        );
    }
}

function mapStateToProps(state: AppState) {
    return {
        accessKey: state.settings.accessKey
    }
}

export default connect(mapStateToProps)(LegacyTab);