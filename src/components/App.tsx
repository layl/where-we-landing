import React, { FC, useState, useEffect } from "react";
import { Box, Center } from "@chakra-ui/core";
import { useDispatch } from "react-redux";

import AppBar from "src/components/AppBar";
import LegacyTab from "src/components/LegacyTab";
import SettingsTab from "src/components/SettingsTab";
import store from "src/store/store";
import { StoredData, UserData } from "src/utils/user_data";
import { setAccessKey, setIcaoPositions } from "src/store/actions";
import MapTab from "src/components/map/MapTab";
import { loadIcaos } from "src/utils/icaos";

const AppTab: FC<{ active: boolean, onClick: () => void }> = (props) => {
    const bgColor = props.active ? "blue.600" : "";
    const hoverBgColor = props.active ? "" : "blue.600";

    return (
        <Center
            as="button"
            bg={bgColor}
            px={2}
            display="inline-block"
            onClick={props.onClick}
            _hover={{ bg: hoverBgColor }}
            _focus={{ outline: "none" }}
        >
            {props.children}
        </Center>
    );
}

const App: FC<{}> = (props) => {
    const dispatch = useDispatch();

    const [activeTab, setActiveTab] = useState<string>("map");

    useEffect(() => {
        (async () => {
            // Load and store the ICAO positions
            const icaoPositions = await loadIcaos();
            dispatch(setIcaoPositions(icaoPositions));
        })();
    });

    // Load in the save file and subscribe for storing when things change
    async function onDataChanged() {
        const state = store.getState();

        const data: StoredData = {
            settings: {
                accessKey: state.settings.accessKey
            }
        };

        await UserData.saveData(data);
    }

    useEffect(() => {
        (async () => {
            const data = await UserData.loadData();
            dispatch(setAccessKey(data.settings?.accessKey ?? null));
        })();

        return store.subscribe(onDataChanged);
    }, [])

    let activeTabComponent;
    switch (activeTab) {
        case "map":
            activeTabComponent = <MapTab />
            break;
        case "settings":
            activeTabComponent = <SettingsTab />
            break;
        case "legacy":
            activeTabComponent = <LegacyTab />
            break;
    }

    return (
        <Box cursor="default" userSelect="none" height="100%" display="flex" flexDirection="column">
            <AppBar />
            <Box bg="blue.500" fontSize={14} px={2} color="white" boxShadow="md">
                <AppTab active={activeTab == "map"} onClick={() => setActiveTab("map")}>
                    MAP
                </AppTab>
                <AppTab active={activeTab == "settings"} onClick={() => setActiveTab("settings")}>
                    SETTINGS
                </AppTab>
                <AppTab active={activeTab == "legacy"} onClick={() => setActiveTab("legacy")}>
                    LEGACY
                </AppTab>
            </Box>
            <Box overflow="auto" flexGrow={1}>
                {activeTabComponent}
            </Box>
        </Box>
    );
}

export default App;