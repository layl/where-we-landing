import React, { FunctionComponent } from "react";
import ReactDOM from "react-dom";
import { ChakraProvider, CSSReset } from "@chakra-ui/core";
import { Provider } from "react-redux";

import App from "src/components/App";
import theme from "src/utils/theme";
import store from "src/store/store";

import "leaflet/dist/leaflet.css";
import "src/index.css";

// This code is needed to properly load the images in the Leaflet CSS
import L from "leaflet";
delete (L.Icon.Default.prototype as any)._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: "../static/leaflet/marker-icon-2x.png",
    iconUrl: "../static/leaflet/marker-icon.png",
    shadowUrl: "../static/leaflet/marker-shadow.png",
});

const WrappedApp: FunctionComponent = (props) => {
    return (
        <ChakraProvider theme={theme}>
            <CSSReset />
            <Provider store={store}>
                <App />
            </Provider>
        </ChakraProvider>
    );
}

ReactDOM.render(<WrappedApp />, document.getElementById("app"));