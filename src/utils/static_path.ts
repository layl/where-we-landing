import path from "path";

declare const STATIC_PATH: string;

let staticPath: string;

if (process.env.NODE_ENV !== "development") {
    staticPath = path.join(__dirname, '/static').replace(/\\/g, '\\\\');
} else {
    staticPath = STATIC_PATH;
}

export default staticPath;