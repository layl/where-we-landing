import fs from "fs";
import staticPath from "./static_path";

export type IcaoEntry = {
    latitude: number;
    longitude: number;
    name: string;
}

export async function loadIcaos() {
    // Read the icao data file
    const data = await fs.promises.readFile(
        staticPath + "/icaodata.csv",
        { encoding: "utf8" }
    );

    // Parse into rows
    const lines = data.split("\n");
    lines.shift();

    // Process rows into entries
    let icaoEntries: Record<string, IcaoEntry> = {};
    for (const line of lines) {
        if (line == "") {
            continue;
        }

        // Extract the values
        const values = line.split(",");
        const icao = values[0];
        const latitude = values[1];
        const longitude = values[2];
        const name = values[5];

        icaoEntries[icao] = {
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
            name: name,
        };
    }
    
    return icaoEntries;
}
